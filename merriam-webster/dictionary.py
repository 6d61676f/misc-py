#!/usr/bin/env python3
import xml.dom.minidom
import requests
import sys


class Termen:

    def __init__(self, termen):
        self._termen = termen
        self._definitii = []
        self._sinonime = []
        self._KEY = ''
        self._URL = 'http://www.dictionaryapi.com/api/v1/references/collegiate/xml/{0}?key={1}'

    def parse(self):

        r = requests.get(self._URL.format(self._termen, self._KEY))
        DOMtree = xml.dom.minidom.parseString(r.text)
        collection = DOMtree.documentElement
        defi = collection.getElementsByTagName('dt')

        for d in defi:
            definitie = str()
            for ex in d.childNodes:
                if ex.nodeType is xml.dom.Node.TEXT_NODE:
                    definitie += ex.data
                elif ex.nodeType is xml.dom.Node.ELEMENT_NODE and ex.tagName == 'sx':
                    self._sinonime.append(ex.childNodes[0].data)
                elif ex.nodeType is xml.dom.Node.ELEMENT_NODE and ex.tagName == 'd_link':
                    definitie += ex.childNodes[0].data
            definitie = definitie.strip()
            definitie = definitie.replace(':','')
            if len(definitie)>1:
                self._definitii.append(definitie)

    def printare(self):
        if(len(self._definitii) >= 1):
            print("\nPentru termenul '{}' avem definitiile:\n".format(self._termen))
            for d in self._definitii:
                print("'{}'".format(d))

        if(len(self._sinonime) >= 1):
            print('\n,iar sinonime:')
            for s in self._sinonime:
                print("'{}'".format(s))


def main():
    termeni = []
    for t in range(1, len(sys.argv)):
        termeni.append(Termen(sys.argv[t]))

    for t in termeni:
        t.parse()
        t.printare()

if __name__ == '__main__':
    main()
