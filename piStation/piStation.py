#!./venv/bin/python2
import serial
import sys
import sleekxmpp
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="input",
                  help="read data from FILE", metavar="/dev/ttyACM*")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-b", "--baudrate", dest="baudrate",
                  help="BAUDRATE for FILE", default=9600)

(options, args) = parser.parse_args()


class SendMsgBot(sleekxmpp.ClientXMPP):

    def __init__(self, jid, password, recipient, msg):
        super(SendMsgBot, self).__init__(jid, password)

        self.recipient = recipient
        self.msg = msg

        self.add_event_handler('session_start', self.start)

    def start(self, event):
        self.send_presence()
        self.get_roster()
        self.send_message(mto=self.recipient, mbody=self.msg)
        self.disconnect(wait=True)

USER = 'notificare@dukgo.com'
PSSWD = ''


if options.input == None:
    parser.print_help()
    sys.exit(1)


try:
    serial = serial.Serial(options.input, options.baudrate)
    while 1:
        c = serial.readline()
        c = c.strip()
        c = 'Sunt ' + c + ' grade Celsius la Pewe'

        xmppNutzy = SendMsgBot(USER, PSSWD, 'nutzy@dukgo.com', c)
        xmppKewe = SendMsgBot(USER, PSSWD, 'kewe@dukgo.com', c)

        if xmppNutzy.connect(('dukgo.com', 5222)):
            xmppNutzy.process(block=True)
            print 'Trimis Nutzy'
        else:
            print 'Error'
        if xmppKewe.connect(('dukgo.com', 5222)):
            xmppKewe.process(block=True)
            print 'Trimis Kewe'

        if (options.verbose):
            sys.stdout.write(c)

except KeyboardInterrupt:
    print "Closing"
    sys.exit(0)
except Exception:
    print "Eroare la deschidere "
    raise
