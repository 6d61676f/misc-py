#!/usr/bin/env python3
import sys
import serial
import argparse

parser = argparse.ArgumentParser(
    description="Simple program to read serial input from Arduino")
parser.add_argument(
    "-q", "--quiet", help="display incoming data", action="store_true")
parser.add_argument("input", help="read from input file",
                    metavar="/dev/ttyACM*")
parser.add_argument("-b", "--baudrate", help="RX Baudrate",
                    type=int, default=9600)
parser.add_argument("-o", "--output", help="output file")
args = parser.parse_args()

try:
    serialDevice = serial.Serial(args.input, args.baudrate)
    fout = None
    if (args.output):
        fout = open(args.output, 'w')
    while 1:
        c = serialDevice.read()
        if (fout):
            fout.write(c)
        if (args.quiet is False):
            sys.stdout.write(c)

except KeyboardInterrupt:
    if serialDevice:
        serialDevice.close()
    if fout:
        fout.close()

    print("Exiting")
    sys.exit(0)
except Exception as exceptie:
    print("Eroare: {0} {1}".format(type(exceptie), exceptie))
    sys.exit(1)
