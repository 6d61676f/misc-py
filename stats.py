#!/usr/bin/env python3
import os
import re
import subprocess
# from collections import OrderedDict as odict


def cliCom(args=None):
    if args is not None:
        try:
            ret = subprocess.check_output(args)
            print(ret.decode("utf-8"))
        except Exception as err:
            print(err.args)


def printHeader(module=None):
    print(80*'-')
    if module is not None:
        print("{0} {1}".format(int(40 - len(module)/2) * ' ', module))
    else:
        print()
    print(80*'-')


def tail(filePath, lines=20):
    if filePath is None:
        return
    if not os.path.exists(filePath):
        return
    with open(filePath) as fil:
        linii = list(filter(lambda x: not re.match(r'^\s*$', x),
                            fil.readlines()))
        list(map(lambda x: print(x, end=''), linii[-lines:]))


def dnsReq(filePath):
    if filePath is None:
        return list()
    if not os.path.exists(filePath):
        return list()
    with open(filePath) as fil:
        linii = list(filter(lambda x: not re.match(r'^\s*$', x),
                            fil.readlines()))
        linii = list(filter(lambda x: re.match(r'.* reply .*', x), linii))
        linii = list(map(lambda x: ' '.join(x.split()[5:8]), linii))
        # linii = odict(map(lambda x: (x.split()[5],x.split()[7]), linii))
        # linii = list(map(lambda x: ' - '.join([x,linii[x]]), linii))
        return linii
    return list()


def printReport():
    if os.getuid() != 0:
        print("Run as root!")
        exit(1)

    printHeader('Storage')
    cliCom(['df', '-h'])
    printHeader()

    printHeader('Mem MB')
    cliCom(['free', '-m'])
    printHeader()

    printHeader('SSH')
    cliCom(['journalctl', '-n', '20', '-u', 'ssh'])
    printHeader()

    printHeader('DNS Requests')
    req = dnsReq("/var/log/dnsmasq.log")[-20:]
    print(*req, sep='\n')
    printHeader()

    printHeader('DHCP Leases')
    tail("/var/lib/misc/dnsmasq.leases")
    printHeader()

    printHeader('Freeradius')
    tail('/var/log/freeradius/radius.log')
    printHeader()

    printHeader('HostAPD')
    cliCom(['journalctl', '-n', '20', '-u', 'hostapd'])
    printHeader()

    printHeader('Warnings')
    cliCom(['journalctl', '-n', '30', '-p', '0..5'])
    printHeader()


if __name__ == "__main__":
    printReport()
