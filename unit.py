import unittest
import math
import os

def lineCounter(textBloc):
    return sum(1 for _ in textBloc.split())

def loadFile(fisier):
    with open(fisier,mode="rt") as wololo:
        fis = wololo.read()
    return fis

class Laur(unittest.TestCase):

    def setUp(self):
        self.linii=['Nu\n','Ma\n','Iupi\n']
        self.fisier = '/tmp/temp.txt'
        with open(self.fisier,mode="wt") as f:
            f.writelines((linie + '\n') for linie in self.linii)

    def tearDown(self):
        if os.path.isfile(self.fisier):
            os.remove(self.fisier)

    def test_fmm(self):
        self.assertEqual("FMM","FMM")

    def test_eroare(self):
        with self.assertRaises(ValueError):
            math.log(-1)

    def test_fisierInexistent(self):
        with self.assertRaises(IOError):
            loadFile(str())

    def test_Linii(self):
        bloc = loadFile(self.fisier)
        count = lineCounter(bloc)
        self.assertEqual(count,3)

if __name__ == '__main__':
    unittest.main()
